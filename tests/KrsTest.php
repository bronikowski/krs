<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KrsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testKRSLookup()
    {
        $this->assertTrue(
            (\bronikowski\KrsApi\KRS::byNIP('7252084098')->adres == \bronikowski\KrsApi\KRS::byKRS('0000548749')->adres)
        );
    }
}
